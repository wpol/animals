package generic.Zad;

import java.util.List;

public class AnimalCollection<T extends Animal> {

    List<Animal> animalList;

    public AnimalCollection(List<Animal> animalList) {
        this.animalList = animalList;
    }

    public void addAnimal(Animal name) {
        animalList.add(name);
    }

    public void removeAnimal(int number) {
        animalList.remove(number);
    }

    public void moveAll() {
        for (Animal animal : animalList) {
            animal.move();
        }
    }
}
