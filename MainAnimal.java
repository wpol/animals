package generic.Zad;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class MainAnimal {
    public static void main(String[] args) {
        menu();
    }

    private static void menu() {
        boolean exit;
        do {
            exit = false;
            System.out.println("1. Add dog");
            System.out.println("2. Add cat");
            System.out.println("3. Remove animal");
            System.out.println("4. Move all animals");
            System.out.println("5. Exit");
            String choice = scanner.next();
            switch (choice) {
                case "1":
                    addDog();
                    break;
                case "2":
                    addCat();
                    break;
                case "3":
                    removeAnimal();
                    break;
                case "4":
                    moveAnimals();
                    break;
                case "5":
                    exit = true;
            }
        }
        while (!exit);
    }

    static List<Animal> animalList = new ArrayList<>();

    static AnimalCollection animalCollection = new AnimalCollection(animalList);

    static Scanner scanner = new Scanner(System.in);

    private static void addCat() {
        System.out.println("Give name");
        String name = scanner.next();
        animalCollection.addAnimal(new Cat(name));
    }

    private static void addDog() {
        System.out.println("Give name");
        String name = scanner.next();
        animalCollection.addAnimal(new Dog(name));
    }

    private static void removeAnimal() {
        boolean isNotProper;
        do {
            isNotProper = false;

            try {
                System.out.println("Give number of an animal to remove");
                int number = scanner.nextInt();
                animalCollection.removeAnimal(number);
            } catch (InputMismatchException e) {
                System.out.println("It isn't a number - try again");
                isNotProper = true;
                scanner.next();
            } catch (IndexOutOfBoundsException e) {
                System.out.println("You gave wrong number - try again");
                isNotProper = true;
            }
        }
        while (isNotProper);
    }

    private static void moveAnimals() {
        animalCollection.moveAll();
    }
}
